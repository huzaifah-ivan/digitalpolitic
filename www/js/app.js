var $$ = Dom7;
var serverHost = "http://103.41.207.226:8080/baseapp/";
var tokenUser;
var app = new Framework7({
  root: '#app', // App root element

  id: 'id.co.dnastudio.dompu', // App bundle ID
  name: 'DigitalPolitik', // App name
  theme: 'auto', // Automatic theme detection

  // App root data
  data: function () {
    return {
      user: {
        firstName: 'John',
        lastName: 'Doe',
      },
      // Demo products for Catalog section
      products: [
        {
          id: '1',
          title: 'Apple iPhone 8',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
        },
        {
          id: '2',
          title: 'Apple iPhone 8 Plus',
          description: 'Velit odit autem modi saepe ratione totam minus, aperiam, labore quia provident temporibus quasi est ut aliquid blanditiis beatae suscipit odio vel! Nostrum porro sunt sint eveniet maiores, dolorem itaque!'
        },
        {
          id: '3',
          title: 'Apple iPhone X',
          description: 'Expedita sequi perferendis quod illum pariatur aliquam, alias laboriosam! Vero blanditiis placeat, mollitia necessitatibus reprehenderit. Labore dolores amet quos, accusamus earum asperiores officiis assumenda optio architecto quia neque, quae eum.'
        },
      ],
      userLoged :{}
    };
  },
  // App root methods
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
    takePic : function() { 
         navigator.camera.getPicture(onSuccess, onFail, {  
            quality: 50, 
            destinationType: Camera.DestinationType.DATA_URL 
         });  
         
         function onSuccess(imageData) { 
            var image = document.getElementById('myImage'); 
            image.src = "data:image/jpeg;base64," + imageData; 
         }  
         
         function onFail(message) { 
            alert('Failed because: ' + message); 
         } 
      },
  },
  // App routes
  routes: routes,


  // Input settings
  input: {
    scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
    scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron,
  },
  // Cordova Statusbar settings
  statusbar: {
    iosOverlaysWebView: true,
    androidOverlaysWebView: false,
  },
  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);
      }
    },
  },
});
//logout
$$('#btn-logout').on('click', function () {
  $$('#my-login-screen [name="username"]').val("");
  $$('#my-login-screen [name="password"]').val("");
  app.form.storeFormData("userLogin");
  app.form.storeFormData("serverHost");
  app.form.storeFormData("userToken");
  // app.request.setup({
  //           headers: {
  //               'Authorization': ''
  //             }
  //           });
  app.loginScreen.open('#my-login-screen');
});
// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  if(username==""){
    app.dialog.alert("Username tidak boleh Kosong",'Error');
    $$('#my-login-screen [name="username"]').focus();
    return false;
  }


  if(password==""){
    app.dialog.alert("Password tidak boleh Kosong",'Error');
    $$('#my-login-screen [name="password"]').focus();
    return false;
  }

  

  // Show Preloader
  app.preloader.show();
  app.request({
    headers: {
      'Content-Type':'application/json'

      },
    url: serverHost+"api/v1/auth/masuk",
    dataType: 'json',
    contentType: "application/json",
    method: 'POST',
    data: { userKontak1:username, userPassword: password },
    statusCode: {
        404: function (xhr) {
            console.log("URL not found");
            app.dialog.alert("URL not found");
        // Hide Preloader
        app.preloader.hide();
        },
        400: function (xhr) {
            app.dialog.alert("Bad request. Some of the inputs provided to the request aren't valid.");
            console.log("Bad request. Some of the inputs provided to the request aren't valid.");
        // Hide Preloader
        app.preloader.hide();
        },
        401: function (xhr) {
            app.dialog.alert("Tidak Dapat Masuk. Usernama/Password anda tidak valid.!!!");
            console.log("Not authenticated. The user session isn't valid.");
        // Hide Preloader
        app.preloader.hide();
        },
        403: function (xhr) {
            app.dialog.alert("The user isn't authorized to perform the specified request.");
            console.log("The user isn't authorized to perform the specified request.");
        // Hide Preloader
        app.preloader.hide();
        },
        500: function (xhr) {
            app.dialog.alert("Internal server error. Additional details will be contained on the server logs.");
            console.log("Internal server error. Additional details will be contained on the server logs.");
        // Hide Preloader
        app.preloader.hide();
        },
        201: function (xhr) {
            app.dialog.alert("The requested resource has been created.");
            console.log("The requested resource has been created.");
        // Hide Preloader
        app.preloader.hide();
        }
    },
    success: function (data, status, xhr) {
        console.log("login ok"+JSON.stringify(data));
        if(data.statusOk){
          app.loginScreen.close('#my-login-screen');

          tokenUser = data.data.userToken;

          app.form.storeFormData("userToken", tokenUser);
          app.form.storeFormData("userLogin", data.data);
          app.form.storeFormData("serverHost", data.data.userHostApi);
          app.userLoged = data.data;

          // Hide Preloader
          app.preloader.hide();
        }else{

          app.dialog.alert('Gagal Login Karena : ' + data.message,'Gagal Login');
          app.preloader.hide();
        }
    },
    error: function (xhr, status) {
        console.log(JSON.stringify(xhr));
          app.loginScreen.close('#my-login-screen');
        app.dialog.alert('Gagal Login Cek Koneksi Internet Anda','Gagal Login');
       // app.loginScreen.close('#my-login-screen');
        console.log("ERROR" +status);
        // Hide Preloader
        app.preloader.hide();
    }
});




});