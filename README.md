# DigitalPolitik

## Framework7 CLI Options

Framework7 app created with following options:

```
{
  "cwd": "/Users/muhammadhuzaifah/Documents/workspace/mobile-app/digitalpolitik",
  "type": [
    "web",
    "cordova"
  ],
  "name": "DigitalPolitik",
  "framework": "core",
  "template": "tabs",
  "bundler": false,
  "cssPreProcessor": false,
  "theming": {
    "customColor": true,
    "color": "#005bbe",
    "darkTheme": false,
    "iconFonts": true,
    "fillBars": true
  },
  "customBuild": false,
  "pkg": "id.co.dnastudio.dompu",
  "cordova": {
    "folder": "cordova",
    "platforms": [
      "ios",
      "android"
    ],
    "plugins": [
      "cordova-plugin-statusbar",
      "cordova-plugin-keyboard",
      "cordova-plugin-splashscreen"
    ]
  }
}
```

## NPM Scripts

* 🔥 `start` - run development server
* 🔧 `serve` - run development server
* 📱 `build-cordova` - build cordova app
* 📱 `build-cordova-ios` - build cordova iOS app
* 📱 `build-cordova-android` - build cordova Android app
## Cordova

Cordova project located in `cordova` folder. You shouldn't modify content of `cordova/www` folder. Its content will be correctly generated when you call `npm run cordova-build-prod`.



## Assets

Assets (icons, splash screens) source images located in `assets-src` folder. To generate your own icons and splash screen images, you will need to replace all assets in this directory with your own images (pay attention to image size and format), and run the following command in the project directory:

```
framework7 assets
```

Or launch UI where you will be able to change icons and splash screens:

```
framework7 assets --ui
```

## Documentation & Resources

* [Framework7 Core Documentation](https://framework7.io/docs/)



* [Framework7 Icons Reference](https://framework7.io/icons/)
* [Community Forum](https://forum.framework7.io)

## Support Framework7

Love Framework7? Support project by donating or pledging on patreon:
https://patreon.com/vladimirkharlampidi







## API DOCUMENTATION

- LOGIN
URL : POST http://data.jarapasaka.com:8080/baseapp/api/v1/auth/masuk
PARAMETER :
```
{
  "userKontak1" : "USER",
  "userPassword": "PASSWORD"
}
```
RESULT:

```
SUKSES RETURN :
{
    "draw": null,
    "start": null,
    "length": null,
    "panjangDisplay": null,
    "order": null,
    "search": null,
    "data": {
        "userId": 1,
        "userNama": "test_user",
        "userPassword": "RxBG2MtxobHWt3tXpFL4zg==",
        "userPasswordConfirm": null,
        "userEmail": "mhuzaifah.ivan@gmail.com",
        "userTglLahir": null,
        "userKontak1": "test_user",
        "userKontak2": null,
        "userKontak3": null,
        "userTokenValid": null,
        "userTokenValidEnd": null,
        "userAvatar": null,
        "userImage": null,
        "userTpsKec": "DOMPU",
        "userTpsKel": "BALI",
        "userTpsNomor": 11,
        "userType": null,
        "userKodeValidasi": null,
        "userRole": "90",
        "userLock": null,
        "userWatch": null,
        "userActive": true,
        "userCreatedBy": null,
        "userCreatedDate": null,
        "userLastActionBy": null,
        "userLastActionDate": null,
        "userOptional": null,
        "userIsSubmited": null,
        "userHostApi": "http://103.41.207.226:8080/baseapp/",
        "userLoginAttempt": null,
        "userParentId": null,
        "userLevel": null,
        "userMinEntryDpt": null,
        "userTpsId": 2,
        "userTpsKelId": null,
        "userToken": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkbmFzdHVkaW8iLCJpc3MiOiJ0ZXN0X3VzZXIiLCJleHAiOjE2MDU5NzE2ODEsInVzZXJJZCI6InRlc3RfdXNlciIsImlhdCI6MTYwNTk3MTU4MSwianRpIjoiMSJ9.wjPhhCQYDTgV_j34EUcoGyyCA3UFs3GdC5JE8MNOGVk",
        "listRole": null,
        "listRoleId": null,
        "tps": {
            "tpsId": 2,
            "tpsLokasiId": 9,
            "tpsNomor": 1,
            "tpsJumlahDpt": null,
            "tpsJumlah": null,
            "tpsPj": null,
            "tpsJumlahLaki": 147,
            "tpsJumlahPerempuan": 172,
            "tpsJumlahTotal": null,
            "tpsTargetEksekusi": null,
            "tpsTargetSuaraSah": null,
            "tpsJumlahPenggerak": null,
            "tpsJumlahPjTimTps": null,
            "tpsKecamatan": "DOMPU",
            "tpsKelurahanDesa": "BADA",
            "jumlahEntryDataPemilih": null,
            "tpsJumlahDataMasukHariIni": null,
            "tpsJumlahPemilihGanda": null,
            "tpsJumlahNonDpt": null,
            "rekapSuaraPaslon1": null,
            "rekapSuaraPaslon2": null,
            "rekapSuaraPaslon3": null,
            "rekapSuaraTidakSah": null
        }
    },
    "message": null,
    "statusOk": true
}

ERROR

{
    "draw": null,
    "start": null,
    "length": null,
    "panjangDisplay": null,
    "order": null,
    "search": null,
    "data": null,
    "message": "User/Password tidak ditemukan!",
    "statusOk": false
}

setiap return 200 (OK), return dari API cek lagi field statusOk nya. kalo dia false, ambil field message untuk pesan errornya
```

- SUBMIT REKAPITULASI
URL : POST http://data.jarapasaka.com:8080/baseapp/api/v1/digipol/vote/rekapitulasi
TOKEN AMBIL DARI JSON HASIL LOGIN (data.userToken). KIRIM HEADER DENGAN format : bearer<space>TOKEN_HASIL_LOGIN
PARAMETER : 
```
{
    "rekapTpsId":2, (ambil dari json hasil login : data.tps.tpsId)
    "rekapSuaraPaslon1": 10,
    "rekapSuaraPaslon2": 18,
    "rekapSuaraPaslon3": 20,
    "rekapSuaraTidakSah": 4,
    "rekapImage" : "image base64"
}
```

result :
```
{
    "draw": null,
    "start": null,
    "length": null,
    "panjangDisplay": null,
    "order": null,
    "search": null,
    "data": null,
    "message": "Berhasil Dikirim",
    "statusOk": true
}
```



